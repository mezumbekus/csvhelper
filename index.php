<?php 

class CSVHelper {


	
	private $data_people;
	private $dirfd;
	const PEOPLE_FILE = 'people.csv';
	const TEXT_DIR = 'texts';
	const OUTPUT_DIR = 'output_texts';
	private $runMethod;
	public function __construct($delim = 'semicolon',$runMethod = 'countAverageLineCount') {

		$temp_arr =  file(self::PEOPLE_FILE);
		if($delim == 'semicolon')
			$delim = ';';
		else if($delim == 'comma')
			$delim = ',';
		else 
		{
			echo "Wrong DELIMITER!!!!";die;
		}

		

		$this->runMethod = $runMethod;

		foreach($temp_arr as $item) {
			$this->data_people[] = str_getcsv($item,$delim);
		}
	 	$this->dirfd = opendir(self::TEXT_DIR);
	 
	}

	private function countAverageLineCount() {
	
		foreach($this->data_people as $user) {

			echo $user[1] . ' - ' . $this->strCount($user[0]) . ' строк \n';
		}
	}

	private function replaceDates () {

		
		while(($file = readdir($this->dirfd))) {
			$file2 = self::TEXT_DIR.'/'.$file;
			if(!is_dir($file2) && preg_match('/^\d+-/', $file)){
			
	 			$text = file_get_contents($file2);
	 			$text = preg_replace_callback('~(\d{2}/\w{2}/\d{2})~', function($ms){ return date('d-m-Y',strtotime($ms[1])); }, $text);
	 			file_put_contents(self::OUTPUT_DIR.'/'.$file, $text);
	 		}
		}
	}

	private function strCount($id) {
		$count = $tmp = 0;
		
		while(($file = readdir($this->dirfd))) {
			$file2 = self::TEXT_DIR.'/'.$file;
	 	if(!is_dir($file2) && preg_match('/^'.$id.'-/', $file)){

	 		$temp = file($file2);
	 		$tmp += count($temp);
	 		$count++;
	 	}
	 		
	 	}
	 	if($count)
	 		return (int)($tmp/$count);
	 	return 0;
	}

	public function run() {
	
		if(strcmp($this->runMethod,'countAverageLineCount') == 0)
			$this->countAverageLineCount();
		else if(strcmp($this->runMethod,'replaceDates') == 0)
			$this->replaceDates();

		
	}
	
}

$test = new CSVHelper($argv[1],$argv[2]);
$test->run();
